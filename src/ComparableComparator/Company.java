package ComparableComparator;

public class Company implements Taxable {
	String name;
	double in;
	double out;
	
	public Company(String n,double i,double o){
		name = n;
		in = i;
		out = o;
	}
	public String getName() {
		return name;
	}
	public double getIn() {
		return in;
	}
	public double getOut() {
		return out;
	}
	public double getProfit() {
		return in - out;
	}
	@Override
	public double getTax() {
		double val = in-out;
		double ans = (val*30)/100;
		return ans;
	}

}
